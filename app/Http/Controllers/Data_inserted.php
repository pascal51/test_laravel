<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 
use App\Models\Data_inserted as Data_inserted_model;

class Data_inserted extends Controller
{
    
    function show(){
        $data =  Data_inserted_model::where('deleted_at','=',null)->get(); 
        return view('welcome',['data_inserted'=>$data]);
    }

    function remove($id){
 
        $data = Data_inserted_model::whereId($id)->get();
        if(!empty($data)){

            $dataUpdate = [
                'deleted_at' => now(), 
            ];

            $query = Data_inserted_model::where('id','=',$id)->update($dataUpdate);

            if($query){
                return back()->with('success','data have been successfully updated');
            }else{
                return back()->with('fail','something went wrong');
            } 

        }else{  
            echo "empty";
        }
        
    }

    function edit($id){

        $data =  Data_inserted_model::whereId($id)->get();
        return view('edit',['data_inserted'=>$data]);
        
    }

    function create(Request $request){

        $request->validate([
            'add_input'=>'',
        ]);

        $query = Data_inserted_model::insert([
            'name'=>$request->input('add_input'),
            'created_at'=>now(),
        ]);

        if($query){
            return back()->with('success','data have been successfully inserted');
        }else{
            return back()->with('fail','something went wrong');
        }
    }

    function update(Request $request){

        $request->validate([
            'id'=>'Required',
            'name'=>'',
        ]);

        // print_r($request->input()); 

        $query = Data_inserted_model::whereId($request->input('id'))->update([
            'name'=>$request->input('name'), 
        ]);

        if($query){
            return redirect('/')->with('success','data have been successfully updated');
        }else{
            return redirect('/')->with('fail','something went wrong');
        }
    }

    function instant_update(Request $request){

        $request->validate([
            'id'=>'Required',
            'name'=>'', 
        ]);

        $query = Data_inserted_model::whereId($request->input('id'))->update([
            'name'=>$request->input('name'),
            'updated_at'=>now(),
        ]);

        echo json_encode($query);

    }

}
