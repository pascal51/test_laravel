<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Data_inserted;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Data_inserted::class,'show'] );
Route::get('remove/{id}', [Data_inserted::class,'remove'] );
Route::get('edit/{id}', [Data_inserted::class,'edit'] );

Route::view('form','welcome');
Route::view('/test_vue','test_vue');

Route::post('submit','App\Http\Controllers\Data_inserted@create');
Route::post('edit/update','App\Http\Controllers\Data_inserted@update');

/////////////////////////////////////// AJAX
Route::get('instant_update','App\Http\Controllers\Data_inserted@instant_update'); 
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
